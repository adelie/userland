# Userland

This project is a port of NetBSD userland utilities to the musl implementation of the C standard library. It aims to provide a complete POSIX userland, with support for user-friendly extensions regardless of origin.

# Rationale

There are a multitude of other projects which aim to provide a Linux userland. None of them are POSIX-compliant, and many have no interest in becoming 100% POSIX-compliant. Some of those projects additionally do not wish to support extensions provided by other projects.

# License

All code in this repository is licensed under `BSD-3-Clause`; see `LICENSE` for a copy of the license text.

# Building

To build `userland`, you will need:

* gprbuild
* libbsd and development headers
* `fts` support library for musl
* `utmps` (required for `who(1)`), and therefore `skalibs`
* byacc

`make` and `make install` will do as you expect. It is *strongly* recommended that you provide `DESTDIR` so as to avoid clobbering existing userland utilities.

# Contributing

Contributions may be made in several ways:

* Write code.
* Write manual pages.
* Write ancillary documentation.
* File tickets in the issue tracker for features which are missing but implemented by another project.
* Participate constructively in project discussions.

